import argparse
import os
import sys
import csv

import tensorflow as tf
import numpy as np
from data_helpers import read_data
from data_helpers import summarize
from data_helpers import split_data
from data_helpers import next_batch
from data_helpers import create_chunks

FLAGS = None


def train():
	# Import data

	print('Loading data...')
	train_data = read_data(FLAGS.data_dir + '/train.csv')
	test_data = read_data(FLAGS.data_dir + '/test.csv')
	print('Done')

	# Data analysis
	summarize(train_data, name='Training data')
	summarize(test_data, name='Test data')

	# Data preprocessing
	trainX, trainY, devX, devY = split_data(train_data, label_index=0, split=0.99)
	trainX, trainY, devX, devY = trainX.to_numpy(), trainY.to_numpy(
	), devX.to_numpy(), devY.to_numpy()
	trainX = trainX.reshape(len(trainX), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)
	devX = devX.reshape(len(devX), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)
	test_data = test_data.to_numpy().reshape(
	    len(test_data), IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)
	print('Num Training examples:', len(trainX))
	print('Num Development examples:', len(devX))
	print('Num testing examples:', len(test_data))

	trainX, devX, test_data = trainX / 255.0, devX / 255.0, test_data / 255.0
	create_chunks(trainX, trainY, BATCH_SIZE)

	# gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.33)
	# sess = tf.InteractiveSession(config=tf.ConfigProto(gpu_options=gpu_options))
	sess = tf.InteractiveSession()

	x = tf.placeholder(
	    tf.float32, shape=(None, IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS))
	y_ = tf.placeholder(tf.int64, shape=(None, ))

	conv1_weights = tf.Variable(
	    tf.truncated_normal(
	        [5, 5, NUM_CHANNELS, 32],  # 5x5 filter, depth 32.
	        stddev=0.1,
	        seed=SEED,
	        dtype=tf.float32))
	conv1_biases = tf.Variable(tf.zeros([32], dtype=tf.float32))
	conv2_weights = tf.Variable(
	    tf.truncated_normal(
	        [5, 5, 32, 64], stddev=0.1, seed=SEED, dtype=tf.float32))
	conv2_biases = tf.Variable(tf.constant(0.1, shape=[64], dtype=tf.float32))
	fc1_weights = tf.Variable(  # fully connected, depth 512.
	    tf.truncated_normal(
	        [IMAGE_SIZE // 4 * IMAGE_SIZE // 4 * 64, 512],
	        stddev=0.1,
	        seed=SEED,
	        dtype=tf.float32))
	fc1_biases = tf.Variable(tf.constant(0.1, shape=[512], dtype=tf.float32))
	fc2_weights = tf.Variable(
	    tf.truncated_normal(
	        [512, NUM_LABELS], stddev=0.1, seed=SEED, dtype=tf.float32))
	fc2_biases = tf.Variable(
	    tf.constant(0.1, shape=[NUM_LABELS], dtype=tf.float32))

	keep_prob = tf.placeholder(tf.float32)

	conv = tf.nn.conv2d(x, conv1_weights, strides=[1, 1, 1, 1], padding='SAME')
	relu = tf.nn.relu(tf.nn.bias_add(conv, conv1_biases))
	pool = tf.nn.max_pool(
	    relu, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
	conv = tf.nn.conv2d(pool, conv2_weights, strides=[1, 1, 1, 1], padding='SAME')
	relu = tf.nn.relu(tf.nn.bias_add(conv, conv2_biases))
	pool = tf.nn.max_pool(
	    relu, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
	pool_shape = tf.shape(pool)
	reshape = tf.reshape(
	    pool, [pool_shape[0], pool_shape[1] * pool_shape[2] * pool_shape[3]])

	out1 = tf.nn.relu(tf.matmul(reshape, fc1_weights) + fc1_biases)
	hidden1 = tf.nn.dropout(out1, keep_prob, seed=SEED)

	y = tf.matmul(hidden1, fc2_weights) + fc2_biases

	loss = tf.losses.sparse_softmax_cross_entropy(labels=y_, logits=y)
	regularizers = (tf.nn.l2_loss(fc1_weights) + tf.nn.l2_loss(fc1_biases) +
	                tf.nn.l2_loss(fc2_weights) + tf.nn.l2_loss(fc2_biases))

	loss += 5e-4 * regularizers
	tf.summary.scalar('loss', loss)
	global_step = tf.placeholder(tf.float32)
	learning_rate = tf.train.exponential_decay(
	    0.001,  # Base learning rate.
	    global_step * BATCH_SIZE,  # Current index into the dataset.
	    len(trainX),  # Decay step.
	    0.98,  # Decay rate.
	    staircase=True)
	# Use simple momentum for the optimization.
	optimizer = tf.train.AdamOptimizer(learning_rate).minimize(loss)

	# Predictions for the current training minibatch.
	prediction = tf.nn.softmax(y)

	correct_prediction = tf.equal(tf.argmax(prediction, 1), y_)
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	tf.summary.scalar('accuracy', accuracy)
	merged = tf.summary.merge_all()
	train_writer = tf.summary.FileWriter(FLAGS.log_dir + '/train', sess.graph)
	test_writer = tf.summary.FileWriter(FLAGS.log_dir + '/test')

	tf.global_variables_initializer().run()
	for step in range(int(NUM_EPOCHS * len(trainX)) // BATCH_SIZE):
		if step % EVAL_FREQUENCY == 0:
			summary, acc = sess.run(
			    [merged, accuracy],
			    feed_dict={
			        x: devX,
			        y_: devY,
			        keep_prob: 1.0,
			        global_step: step
			    })
			test_writer.add_summary(summary, step)
			print('Devset accuracy at step %s is %s' % (step, acc))
		else:
			xs, ys = next_batch(trainX, trainY, BATCH_SIZE, step)
			summary, _, acc, lr = sess.run(
			    [merged, optimizer, accuracy, learning_rate],
			    feed_dict={
			        x: xs,
			        y_: ys,
			        keep_prob: 0.5,
			        global_step: step
			    })
			train_writer.add_summary(summary, step)
			if step % 99 == 0:
				print('Trainset accuracy at step %s is %s' % (step, acc))
				print('learning_rate:', lr)
	with open('submission.csv', 'w') as f:
		fieldnames = ['ImageId', 'Label']
		writer = csv.DictWriter(f, fieldnames=fieldnames)
		writer.writeheader()
		for i, input_ in enumerate(test_data):
			pred = sess.run(
			    [prediction], feed_dict={
			        x: [input_],
			        keep_prob: 1.0,
			        global_step: 0
			    })
			writer.writerow({'ImageId': i + 1, 'Label': np.argmax(pred[0])})

	train_writer.close()
	test_writer.close()


def main(_):
	if tf.gfile.Exists(FLAGS.log_dir):
		tf.gfile.DeleteRecursively(FLAGS.log_dir)
	tf.gfile.MakeDirs(FLAGS.log_dir)
	with tf.Graph().as_default():
		train()


if __name__ == '__main__':
	IMAGE_SIZE = 28
	NUM_CHANNELS = 1
	PIXEL_DEPTH = 255
	NUM_LABELS = 10
	SEED = 66478  # Set to None for random seed.
	BATCH_SIZE = 64
	NUM_EPOCHS = 20
	EVAL_FREQUENCY = 100
	parser = argparse.ArgumentParser()
	parser.add_argument(
	    '--max_steps',
	    type=int,
	    default=1000,
	    help='Number of steps to run trainer.')
	parser.add_argument(
	    '--learning_rate',
	    type=float,
	    default=0.001,
	    help='Initial learning rate')
	parser.add_argument(
	    '--dropout',
	    type=float,
	    default=0.9,
	    help='Keep probability for training dropout.')
	parser.add_argument(
	    '--data_dir',
	    type=str,
	    default='/tmp',
	    help='Directory for storing input data')
	parser.add_argument(
	    '--log_dir', type=str, default='/tmp', help='Summaries log directory')

	FLAGS, unparsed = parser.parse_known_args()
	tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)